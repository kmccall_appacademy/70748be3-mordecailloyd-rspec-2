def measure(num_times=1)
  i=0
  end_time=[]
  while i < num_times
    start_time=Time.now
    yield
    finish_time=Time.now
    end_time << (finish_time - start_time)
    i+=1
  end
  end_time_average=((end_time.inject(0, :+))/end_time.length)
end
