def reverser()
  words=yield.split(" ")
  i=0
  words = words.map do |word|
    word.reverse
  end
  words=words.join(" ")
  words
end

def adder(add_value=1)
  sum=yield + add_value
end

def repeater(num_times=1)
  i=0
  while i<num_times
    yield
    i+=1
  end
end
