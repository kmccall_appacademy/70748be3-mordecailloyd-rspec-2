# ### Factors
#
# Write a method `factors(num)` that returns an array containing all the
# factors of a given number.


def factors(num)
  fin_array=[]
  i=1
  while i <= num
    if num % i == 0
      fin_array.push(i)
    end
    i+=1
  end
  fin_array
end

# ### Bubble Sort
#
# http://en.wikipedia.org/wiki/bubble_sort
#
# Implement Bubble sort in a method, `Array#bubble_sort!`. Your method should
# modify the array so that it is in sorted order.
#
# > Bubble sort, sometimes incorrectly referred to as sinking sort, is a
# > simple sorting algorithm that works by repeatedly stepping through
# > the list to be sorted, comparing each pair of adjacent items and
# > swapping them if they are in the wrong order. The pass through the
# > list is repeated until no swaps are needed, which indicates that the
# > list is sorted. The algorithm gets its name from the way smaller
# > elements "bubble" to the top of the list. Because it only uses
# > comparisons to operate on elements, it is a comparison
# > sort. Although the algorithm is simple, most other algorithms are
# > more efficient for sorting large lists.
#
# Hint: Ruby has parallel assignment for easily swapping values:
# http://rubyquicktips.com/post/384502538/easily-swap-two-variables-values
# a,b = b,a
# After writing `bubble_sort!`, write a `bubble_sort` that does the same
# but doesn't modify the original. Do this in two lines using `dup`.
#
# Finally, modify your `Array#bubble_sort!` method so that, instead of
# using `>` and `<` to compare elements, it takes a block to perform the
# comparison:
#
# ```ruby
# [1, 3, 5].bubble_sort! { |num1, num2| num1 <=> num2 } #sort ascending
# [1, 3, 5].bubble_sort! { |num1, num2| num2 <=> num1 } #sort descending
# [joe, bob, sally].bubble_sort! { |person1, person2| person1.age <=> person2.age }
# ```
#
# #### `#<=>` (the **spaceship** method) compares objects. `x.<=>(y)` returns
# `-1` if `x` is less than `y`. If `x` and `y` are equal, it returns `0`. If
# greater, `1`. For future reference, you can define `<=>` on your own classes.
#
# http://stackoverflow.com/questions/827649/what-is-the-ruby-spaceship-operator

class Array
  def bubble_sort!( &prc )
    if prc == nil
      prc = Proc.new { |num1, num2| num1 <=> num2 }
    end
    swap = 0
    i = 0
    while (i+1) < self.length
      if prc.call(self[i] , self[i+1]) > 0
        self[i+1] , self[i] = self[i] , self[i+1]
        swap += 1
      end
      i+=1
    end
    if swap > 0
      self.bubble_sort! &prc
    end
    self
  end

  def bubble_sort(&prc)
    dup_array=self.dup
    dup_array.bubble_sort! &prc
  end
end


# ### Substrings and Subwords
#
# Write a method, `substrings`, that will take a `String` and return an
# array containing each of its substrings. Don't repeat substrings.
# Example output: `substrings("cat") => ["c", "ca", "cat", "a", "at",
# "t"]`.
#
# Your `substrings` method returns many strings that are not true English
# words. Let's write a new method, `subwords`, which will call
# `substrings`, filtering it to return only valid words. To do this,
# `subwords` will accept both a string and a dictionary (an array of
# words).

def substrings(string)
  i1=0
  fin_array=[]
  while i1 < string.length
    i2=i1
    while i2 < string.length
      fin_array.push(string[i1..i2])
      i2+=1
    end
    i1+=1
  end
  fin_array
end


def subwords(word, dictionary)
  fin_array=[]
  substrings(word).each do |substring|
    if dictionary.include?(substring)
      fin_array.push(substring)
    end
  end
  fin_array.uniq
end

# ### Doubler
# Write a `doubler` method that takes an array of integers and returns an
# array with the original elements multiplied by two.

def doubler(array)
  fin_array=[]
  array.each do |value|
    value = (value * 2)
    fin_array.push(value)
  end
  fin_array
end

def doubler(array)
  array.map do |value|
    (value * 2)
  end
end


# ### My Each
# Extend the Array class to include a method named `my_each` that takes a
# block, calls the block on every element of the array, and then returns
# the original array. Do not use Enumerable's `each` method. I want to be
# able to write:
#
# ```ruby
# # calls my_each twice on the array, printing all the numbers twice.
# return_value = [1, 2, 3].my_each do |num|
#   puts num
# end.my_each do |num|
#   puts num
# end
# # => 1
#      2
#      3
#      1
#      2
#      3
#
# p return_value # => [1, 2, 3]
# ```

class Array
  def my_each(&prc)
    i=0
    while i < self.length
      prc.call (self[i])
      i+=1
    end
    self
  end
end

# ### My Enumerable Methods
# * Implement new `Array` methods `my_map` and `my_select`. Do
#   it by monkey-patching the `Array` class. Don't use any of the
#   original versions when writing these. Use your `my_each` method to
#   define the others. Remember that `each`/`map`/`select` do not modify
#   the original array.
# * Implement a `my_inject` method. Your version shouldn't take an
#   optional starting argument; just use the first element. Ruby's
#   `inject` is fancy (you can write `[1, 2, 3].inject(:+)` to shorten
#   up `[1, 2, 3].inject { |sum, num| sum + num }`), but do the block
#   (and not the symbol) version. Again, use your `my_each` to define
#   `my_inject`. Again, do not modify the original array.

class Array
  def my_map(&prc)
    fin_array=[]
    self.my_each { |num1| fin_array.push(prc.call(num1)) }
    fin_array
  end

  def my_select(&prc)
    fin_array=[]
    self.my_each do |num1|
      tmp = prc.call(num1)
      if tmp == true
        fin_array.push(num1)
      end
    end
    fin_array
  end

  def my_inject(&blk)
    fin_value=self[0]
    self[1..-1].my_each { |num1| fin_value=blk.call(fin_value,num1) }
    fin_value
  end

end

# ### Concatenate
# Create a method that takes in an `Array` of `String`s and uses `inject`
# to return the concatenation of the strings.
#
# ```ruby
# concatenate(["Yay ", "for ", "strings!"])
# # => "Yay for strings!"
# ```

def concatenate(strings)
  strings.inject {|fin_string,string| fin_string=(fin_string+string)}
  #  strings.my_inject {|fin_string,string| fin_string=(fin_string+string).join(" ")}
  # for ruby standard of including spaces when calling concatenate on arrays- the rspec however provides spacing for us
end
